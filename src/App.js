import React from 'react';
import './App.css';

import CharacterWithHook from './Component/CharHook';
import MutationWithHook from './Component/MutHook';

function App() {
  return (
    <div className="App">
      <CharacterWithHook />
      {/* <MutationWithHook /> */}
    </div>
  );
}

export default App;
